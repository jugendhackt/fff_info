import 'package:device_calendar/device_calendar.dart';
import 'package:fff_info/i18n.dart';
import 'package:fff_info/theme.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:latlong/latlong.dart';
import 'package:share/share.dart';
import 'package:speech_bubble/speech_bubble.dart';

import 'api.dart';
import 'responsiveDrawerScaffold.dart';

class StrikeListPage extends StatefulWidget {
  StrikeListPage({
    Key key,
  }) : super(key: key);

  @override
  _StrikeListPageState createState() => _StrikeListPageState();
}

class _StrikeListPageState extends State<StrikeListPage>
    with TickerProviderStateMixin {
  List _strikeList = List();
  IconData _appBarIcon = Icons.search;
  TextEditingController _searchController = TextEditingController();
  Set<String> _countries = Set();
  String _selectedCountry = 'All';

  String _query = "";
  DateTime _before;
  DateTime _after = DateTime.now();

  Set _eventTypes = Set();
  bool _onlyShowComingEvents = true;
  bool _onlyShowEventsBefore = false;

  TabController _tabCtrl;

  @override
  void initState() {
    _tabCtrl = TabController(length: 2, vsync: this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (_strikeList.isEmpty) {
      Api().eventData().then((data) {
        data.sort(
            (a, b) => parseDate(a['date']).compareTo(parseDate(b['date'])));
        data.forEach((elem) => _countries.add(elem['country']));
        _countries.removeWhere((value) {
          return value == null || value == '';
        });
        setState(() {
          _strikeList = data;
        });
      });
    }
    List filteredItems = List.from(_strikeList);
    if (_query != "")
      filteredItems = _searchStrikes(filteredItems, query: _query);
    if (_onlyShowEventsBefore)
      filteredItems = _filterByDate(filteredItems, before: _before);
    if (_onlyShowComingEvents)
      filteredItems = _filterByDate(filteredItems, after: _after);
    if (_selectedCountry != 'All')
      filteredItems.removeWhere((elem) {
        return (elem['country'] != _selectedCountry);
      });
    filteredItems = _filterByRecurrence(filteredItems);
    return ResponsiveDrawerScaffold(
      reallyWantToProvideAppBar: AppBar(
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          title: (_appBarIcon == Icons.search)
              ? Text("Strike List")
              : Semantics(
                  label: 'Search strikes by keywords',
                  child: TextField(
                    autofocus: true,
                    controller: _searchController,
                    onChanged: (newVal) {
                      setState(() {
                        _query = newVal;
                      });
                    },
                    //decoration: InputDecoration(labelText: 'Search strikes',),
                  ),
                ),
          actions: <Widget>[
            IconButton(
              tooltip: 'Toggle search field',
              icon: Icon(_appBarIcon),
              onPressed: () {
                if (_appBarIcon == Icons.search) {
                  setState(() {
                    _appBarIcon = Icons.close;
                  });
                } else {
                  setState(() {
                    _appBarIcon = Icons.search;
                    _query = '';
                  });
                }
              },
            )
          ],
          bottom: TabBar(
            controller: _tabCtrl,
            tabs: [
              Tab(
                text: 'List',
              ),
              Tab(
                text: 'Map',
              ),
            ],
          )),
      body: Column(
        children: <Widget>[
          (_strikeList.isNotEmpty)
              ? (filteredItems.isNotEmpty)
                  ? Expanded(
                      child: TabBarView(
                      // ! Required to allow scrolling and moving of map
                      physics: NeverScrollableScrollPhysics(),
                      controller: _tabCtrl,
                      children: <Widget>[
                        Column(
                          children: <Widget>[
                            Text(
                              "Strike List",
                              style: Theme.of(context).textTheme.headline4,
                            ),
                            SizedBox(
                              height: 8,
                            ),
                            Text("Matching events: " +
                                filteredItems.length.toString() +
                                " of " +
                                _strikeList.length.toString() +
                                ' (' +
                                ((filteredItems.length / _strikeList.length) *
                                        100)
                                    .round()
                                    .toString() +
                                '%)'),
                            Expanded(
                              child: ListView.builder(
                                itemCount: filteredItems.length,
                                itemBuilder: (BuildContext context, int index) {
                                  return (StrikeDetailTile(
                                    strikeData: filteredItems[index],
                                  ));
                                },
                              ),
                            ),
                          ],
                        ),
                        FlutterMap(
                          options: MapOptions(
                              center: LatLng(51.5167, 9.9167),
                              zoom: 5.7,
                              minZoom: 4,
                              maxZoom: 19),
                          layers: [
                            TileLayerOptions(
                                urlTemplate:
                                    'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
                                tileProvider: (kIsWeb)
                                    ? NonCachingNetworkTileProvider()
                                    : CachedNetworkTileProvider()),
                            MarkerLayerOptions(
                              markers: filteredItems
                                  .map<Marker>((item) => _generateMarker(item))
                                  .toList(),
                            ),
                          ],
                        )
                      ],
                    ))
                  : Center(
                      child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text('No matching events found.'),
                    ))
              : Center(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: CircularProgressIndicator(),
                  ),
                ),
        ],
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: _toggleFilterMenu,
        label: Text('Filter'),
        icon: Icon(FontAwesomeIcons.filter),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }

  Marker _generateMarker(var item) {
    return Marker(
        width: 80.0,
        height: 80.0,
        point: LatLng(
            item['lat'] is String ? double.parse(item['lat']) : item['lat'],
            item['lon'] is String ? double.parse(item['lon']) : item['lon']),
        builder: (ctx) => GestureDetector(
              onTap: () {
                showDialog(
                    context: context,
                    builder: (context) => Dialog(
                            child: Container(
                          constraints: BoxConstraints(maxWidth: 768),
                          child: StrikeDetail(
                            strikeData: item,
                            hideHeader: false,
                          ),
                        )));
              },
              child: Column(
                children: <Widget>[
                  SpeechBubble(
                    nipLocation: NipLocation.BOTTOM,
                    color: Theme.of(context).primaryColor,
                    borderRadius: 16,
                    child: Center(
                      child: Container(
                        width: 8,
                        height: 8,
                        decoration: const BoxDecoration(
                          color: Colors.white,
                          shape: BoxShape.circle,
                        ),
                      ),
                    ),
                    height: 32,
                    width: 32,
                    padding: const EdgeInsets.all(2),
                  ),
                  // undoing required offset by the SpeechBubble
                  SizedBox(
                    height: 28,
                  )
                ],
              ),
            ));
  }

  List _filterByDate(List events, {DateTime after, DateTime before}) {
    if (before != null) {
      events.removeWhere((item) {
        return (parseDate(item['date']).isAfter(before));
      });
    }
    if (after != null) {
      events.removeWhere((item) {
        bool isBefore = parseDate(item['date']).isBefore(after);
        return (isBefore);
      });
    }
    return (events);
  }

  List _searchStrikes(List events, {String query}) {
    events.removeWhere((strikeItem) {
      parseDate(strikeItem['date']);
      String strikeText = strikeItem['country'] +
          strikeItem['town'] +
          strikeItem['location'] +
          strikeItem['event_type'] +
          strikeItem['contact_name'] +
          strikeItem['notes'] +
          strikeItem['org'];
      return (!strikeText.toLowerCase().contains(query.toLowerCase()));
    });
    return (events);
  }

  Future<Null> _selectBeforeDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: (_before != null) ? _before : DateTime.now(),
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != _before)
      setState(() {
        _before = picked;
      });
  }

  void _toggleFilterMenu() {
    showModalBottomSheet(
        context: context,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(16), topRight: Radius.circular(16)),
        ),
        builder: (context) {
          return (Container(
            child: ListView(
              children: <Widget>[
                ListTile(
                  title: Row(
                    children: <Widget>[
                      Text('Country: '),
                      DropdownButton(
                        value: _selectedCountry,
                        icon: Icon(FontAwesomeIcons.globeAsia),
                        iconSize: 24,
                        elevation: 16,
                        style: TextStyle(
                            color: ClimateTheme.of(context).primaryColor),
                        underline: Container(
                          height: 2,
                          color: ClimateTheme.of(context).accentColor,
                        ),
                        onChanged: (String newValue) {
                          setState(() {
                            _selectedCountry = newValue;
                          });
                        },
                        items: () {
                          List<String> countries = ['All'];
                          countries.addAll(() {
                            // Just adding ordered list of countries to ['All']
                            var c = _countries.toList();
                            c.sort();
                            return c;
                          }());
                          // Returning Dropdown buttons for each country
                          return countries
                              .map<DropdownMenuItem<String>>((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList();
                        }(),
                      )
                    ],
                  ),
                ),
                Divider(),
                SwitchListTile(
                  title: Text('Only comming'),
                  value: _onlyShowComingEvents,
                  onChanged: (newValue) {
                    if (newValue) {
                      setState(() {
                        _onlyShowComingEvents = true;
                        _after = DateTime.now();
                      });
                    } else {
                      setState(() {
                        _onlyShowComingEvents = false;
                        _after = null;
                      });
                    }
                    Navigator.pop(context);
                  },
                ),
                SwitchListTile(
                  title: Row(
                    children: <Widget>[
                      Text('Only before: '),
                      RaisedButton(
                        child: Text((_before == null)
                            ? 'Not selected'
                            : "${_before.toLocal()}"),
                        onPressed: () {
                          _selectBeforeDate(context);
                        },
                      ),
                    ],
                  ),
                  value: _onlyShowEventsBefore,
                  onChanged: (newValue) {
                    if (newValue) {
                      setState(() {
                        _onlyShowEventsBefore = true;
                      });
                    } else {
                      setState(() {
                        _onlyShowEventsBefore = false;
                        _after = null;
                      });
                    }
                    Navigator.pop(context);
                  },
                ),
                Divider(),
                ListTile(
                  leading: Icon(FontAwesomeIcons.retweet),
                  title: Text('Recurrence:'),
                ),
                CheckboxListTile(
                  title: Text('Every Friday'),
                  value: _eventTypes.contains('Every Friday'),
                  onChanged: (selected) {
                    setState(() {
                      (selected)
                          ? _eventTypes.add('Every Friday')
                          : _eventTypes.remove('Every Friday');
                    });
                    Navigator.pop(context);
                  },
                ),
                CheckboxListTile(
                  title: Text('Weekly'),
                  value: _eventTypes.contains('Weekly'),
                  onChanged: (selected) {
                    setState(() {
                      (selected)
                          ? _eventTypes.add('Weekly')
                          : _eventTypes.remove('Weekly');
                    });
                    Navigator.pop(context);
                  },
                ),
                CheckboxListTile(
                  title: Text('Once only'),
                  value: _eventTypes.contains('Once only'),
                  onChanged: (selected) {
                    setState(() {
                      (selected)
                          ? _eventTypes.add('Once only')
                          : _eventTypes.remove('Once only');
                    });
                    Navigator.pop(context);
                  },
                ),
                CheckboxListTile(
                  title: Text('Historic (marked as)'),
                  value: _eventTypes.contains('Historic'),
                  onChanged: (selected) {
                    setState(() {
                      (selected)
                          ? _eventTypes.add('Historic')
                          : _eventTypes.remove('Historic');
                    });
                    Navigator.pop(context);
                  },
                ),
                CheckboxListTile(
                  title: Text(
                    'All',
                    style: TextStyle(fontStyle: FontStyle.italic),
                  ),
                  value: _eventTypes.isEmpty,
                  onChanged: (selected) {
                    setState(() {
                      if (selected) _eventTypes.clear();
                    });
                    Navigator.pop(context);
                  },
                ),
                Divider(),
                Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: Center(
                    child: Text(
                      "Source: https://www.fridaysforfuture.org/events/map",
                      style: Theme.of(context).textTheme.caption,
                    ),
                  ),
                )
              ],
            ),
          ));
        });
  }

  List _filterByRecurrence(List filteredItems) {
    if (_eventTypes.isNotEmpty)
      filteredItems.removeWhere((item) {
        return (!_eventTypes.contains(item['recurrence']));
      });
    return filteredItems;
  }
}

class StrikeDetailTile extends StatelessWidget {
  final Map strikeData;

  StrikeDetailTile({@required this.strikeData});

  @override
  Widget build(BuildContext context) {
    return ExpansionTile(
      title: Text(
        strikeData['country'] + ', ' + strikeData['town'],
        style: Theme.of(context)
            .textTheme
            .headline6
            .copyWith(fontWeight: FontWeight.w500),
      ),
      children: <Widget>[
        Card(
          margin: EdgeInsets.all(8.0),
          child: Padding(
            padding: EdgeInsets.all(8.0),
            child: StrikeDetail(
              strikeData: strikeData,
              hideHeader: true,
            ),
          ),
        )
      ],
    );
  }
}

class StrikeDetail extends StatefulWidget {
  final Map strikeData;
  final bool hideHeader;

  StrikeDetail({@required this.strikeData, this.hideHeader = true});

  @override
  _StrikeDetailState createState() => _StrikeDetailState();
}

class _StrikeDetailState extends State<StrikeDetail> {
  bool _createCalendarEventRecurringly = true;

  @override
  Widget build(BuildContext context) {
    return ListView(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      children: <Widget>[
        if (!widget.hideHeader)
          ListTile(
              leading: Icon(FontAwesomeIcons.globeAsia),
              title: Text(
                widget.strikeData['country'] + ', ' + widget.strikeData['town'],
                style: Theme.of(context).textTheme.headline5,
              )),
        ListTile(
          leading: Icon(FontAwesomeIcons.calendarCheck),
          title: Text((widget.strikeData['recurrence'] == 'Historic')
              ? '[Historic, before recording]'
              : (DateFormat("EEEE, dd.MM.yyyy")
                      .format(parseDate(widget.strikeData['date'])) +
                  ', ' +
                  widget.strikeData['time'])),
          onTap: () => _saveToCalendar(context, widget.strikeData),
        ),
        ListTile(
          leading: Icon(FontAwesomeIcons.mapMarkedAlt),
          title: Text((widget.strikeData['recurrence'] == 'Historic')
              ? (widget.strikeData['country'] +
                  ', ' +
                  widget.strikeData['town'] +
                  ' [No details available, historic event.]')
              : widget.strikeData['location']),
        ),
        ListTile(
          leading: Icon(FontAwesomeIcons.retweet),
          title: Text('Recurrance: ' + widget.strikeData['recurrence']),
        ),
        ListTile(
          leading: Icon(FontAwesomeIcons.hammer),
          title: Text('Event type: ' + widget.strikeData['event_type']),
        ),
        ListTile(
          title: Wrap(
            spacing: 8.0, // gap between adjacent chips
            runSpacing: 4.0, // gap between lines
            alignment: WrapAlignment.center,
            children: <Widget>[
              OutlineButton.icon(
                icon: Icon(Icons.share),
                label: Text('Share this event'),
                onPressed: () {
                  Share.share('Climate strike on ' +
                      ((widget.strikeData['recurrence'] == 'Historic')
                          ? '[Historic event]'
                          : (DateFormat("EEEE, dd.MM.yyyy").format(
                                  parseDate(widget.strikeData['date'])) +
                              ', ' +
                              widget.strikeData['time'])) +
                      '\n\nin ' +
                      widget.strikeData['country'] +
                      ', ' +
                      widget.strikeData['town'] +
                      '\n' +
                      'Event type: ' +
                      widget.strikeData['event_type'] +
                      '\n' +
                      'Recurrance: ' +
                      widget.strikeData['recurrence']);
                },
              ),
              if (!kIsWeb)
                OutlineButton.icon(
                  icon: Icon(FontAwesomeIcons.calendarCheck),
                  label: Text('Save to calendar'),
                  onPressed: () => _saveToCalendar(context, widget.strikeData),
                ),
            ],
          ),
        ),
      ],
    );
  }

  void _saveToCalendar(BuildContext context, Map strikeData) async {
    DeviceCalendarPlugin deviceCalendarPlugin = new DeviceCalendarPlugin();
    // check if the calendar permission exists:
    if (!(await deviceCalendarPlugin.hasPermissions()).data) {
      await deviceCalendarPlugin.requestPermissions();
      if (!(await deviceCalendarPlugin.hasPermissions()).data) {
        showDialog(
            context: context,
            builder: (context) => AlertDialog(
                  actions: <Widget>[
                    MaterialButton(
                      onPressed: () => Navigator.of(context).pop(),
                      child: new Text("Cancel"),
                    ),
                    MaterialButton(
                      onPressed: () => Navigator.of(context).pop(),
                      child: new Text("Allow"),
                    )
                  ],
                  title: Text(Internationalisation.of(context)
                      .translation("allow_cal_access_msg")),
                ));
        return;
      }
    }
    // get calendar:
    List<Calendar> calendars =
        (await deviceCalendarPlugin.retrieveCalendars()).data.toList();
    calendars.removeWhere((c) => c.isReadOnly);

    Widget recurrenceTile = Container();
    if (strikeData['recurrence'] == 'Weekly' ||
        strikeData['recurrence'] == 'Every Friday')
      recurrenceTile = SwitchListTile(
        title: Text('Repeat weekly'),
        subtitle: Text(
            'As this is a weekly-recurring event, you can create it recurringly.'),
        value: _createCalendarEventRecurringly,
        onChanged: (bool newValue) => setState(() {
          _createCalendarEventRecurringly = newValue;
        }),
      );

    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text(Internationalisation.of(context).translation("choose_cal")),
        content: Container(
          width: double.maxFinite,
          child: ListView.builder(
            shrinkWrap: true,
            itemBuilder: (context, index) {
              if (index == 0) return recurrenceTile;
              // Manually parsing date due to: PlatformException(500, allDay is true but sec, min, hour are not 0., null)
              var date = parseDate(strikeData['date']);
              // date = date.toUtc().add(date.timeZoneOffset);
              var end = date
                  .add(Duration(days: 1))
                  .subtract(Duration(microseconds: 1));
              // creating calendar event for the selected strike
              Event calendarEvent = Event(
                calendars[index - 1].id,
                title: "Climate strike in ${strikeData['town']}",
                description:
                    "A Fridays for Future climate ${strikeData['event_type']} in ${strikeData['town']}, ${strikeData['country']}, ${strikeData['location']} at: ${strikeData['time']}",
                start: date,
                end: end,
              );
              // Checking whether to create recurringly
              if (_createCalendarEventRecurringly)
                calendarEvent.recurrenceRule =
                    RecurrenceRule(RecurrenceFrequency.Weekly);
              // Finally adding event to calendar's ListTile
              return ListTile(
                  title: Text(calendars[index - 1].name),
                  onTap: () async {
                    await deviceCalendarPlugin
                        .createOrUpdateEvent(calendarEvent);
                    Navigator.of(context).pop();
                  });
            },
            itemCount: calendars.length + 1,
          ),
        ),
      ),
    );
  }
}

DateTime parseDate(String date) {
  Map monthNames = {
    'Jan': 01,
    'Feb': 02,
    'Mar': 03,
    'Apr': 04,
    'May': 05,
    'Jun': 06,
    'Jul': 07,
    'Aug': 08,
    'Sep': 09,
    'Oct': 10,
    'Nov': 11,
    'Dec': 12,
  };
  try {
    int day = int.parse(date.substring(5, 7));
    int month = monthNames[date.substring(8, 11)];
    int year = int.parse(date.substring(12, 16));

    return DateTime(year, month, day);
  } catch (e) {
    // Fallback for `Historic` events
    return DateTime.now().subtract(Duration(days: (364.25 * 4).toInt()));
  }
}
