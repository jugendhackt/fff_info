import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'globals.dart' as globals;

enum ClimateThemeKeys { GERMANY, WORLD }

class ClimateThemes {
  static final ThemeData germanTheme = ThemeData(
      primaryColor: globals.G_PRIMARY,
      accentColor: globals.G_SECONDARY,
      fontFamily: 'Merriw',
      colorScheme: ColorScheme(
          primary: globals.G_PRIMARY,
          secondary: globals.G_SECONDARY,
          primaryVariant: Color.fromRGBO(0, 146, 75, 1),
          secondaryVariant: Color.fromRGBO(200, 176, 0, 1),
          surface: globals.WHITE,
          background: globals.WHITE,
          error: Colors.red,
          onPrimary: globals.G_BLACK,
          onSecondary: globals.G_BLACK,
          onSurface: globals.G_BLACK,
          onBackground: globals.G_BLACK,
          brightness: Brightness.light,
          onError: globals.WHITE),
      textTheme: TextTheme(
        headline: TextStyle(fontFamily: 'Jost', fontWeight: FontWeight.w500),
        subhead: TextStyle(fontFamily: 'Jost'),
        title: TextStyle(fontFamily: 'Jost', fontWeight: FontWeight.w500),
        button: TextStyle(fontFamily: 'Merriw'),
        display1: TextStyle(fontFamily: 'Jost', fontWeight: FontWeight.w500),
        overline: TextStyle(fontFamily: 'Merriw'),
        display2: TextStyle(fontFamily: 'Jost', fontWeight: FontWeight.w500),
        display3: TextStyle(fontFamily: 'Jost', fontWeight: FontWeight.w500),
        display4: TextStyle(fontFamily: 'Jost', fontWeight: FontWeight.w500),
        subtitle: TextStyle(fontFamily: 'Jost', fontWeight: FontWeight.w500),
        caption: TextStyle(fontFamily: 'Merriw'),
      ));

  static final ThemeData worldTheme = ThemeData(
      primaryColor: globals.W_PRIMARY,
      accentColor: globals.W_SECONDARY,
      fontFamily: 'Katwijk',
      colorScheme: ColorScheme(
          primary: globals.W_PRIMARY,
          secondary: globals.W_SECONDARY,
          primaryVariant: Color.fromRGBO(0, 146, 75, 1),
          secondaryVariant: Color.fromRGBO(200, 176, 0, 1),
          surface: globals.WHITE,
          background: globals.WHITE,
          error: Colors.red,
          onPrimary: globals.W_BLACK,
          onSecondary: globals.W_BLACK,
          onSurface: globals.W_BLACK,
          onBackground: globals.W_BLACK,
          brightness: Brightness.light,
          onError: globals.WHITE),
      textTheme: TextTheme(
        headline: TextStyle(fontFamily: 'Greve', fontWeight: FontWeight.w500),
        subhead: TextStyle(fontFamily: 'Katwijk'),
        title: TextStyle(fontFamily: 'Greve', fontWeight: FontWeight.w500),
        button: TextStyle(fontFamily: 'Klima'),
        display1: TextStyle(fontFamily: 'Greve', fontWeight: FontWeight.w500),
        overline: TextStyle(fontFamily: 'Klima'),
        display2: TextStyle(fontFamily: 'Greve', fontWeight: FontWeight.w500),
        display3: TextStyle(fontFamily: 'Greve', fontWeight: FontWeight.w500),
        display4: TextStyle(fontFamily: 'Greve', fontWeight: FontWeight.w500),
        subtitle: TextStyle(fontFamily: 'Greve', fontWeight: FontWeight.w500),
        caption: TextStyle(fontFamily: 'Klima'),
      ));

  static ThemeData getThemeFromKey(ClimateThemeKeys themeKey) {
    switch (themeKey) {
      case ClimateThemeKeys.GERMANY:
        return germanTheme;
      case ClimateThemeKeys.WORLD:
        return worldTheme;
      default:
        return worldTheme;
    }
  }
}

class _ClimateTheme extends InheritedWidget {
  final ClimateThemeState data;

  _ClimateTheme({
    this.data,
    Key key,
    @required Widget child,
  }) : super(key: key, child: child);

  @override
  bool updateShouldNotify(_ClimateTheme oldWidget) {
    return true;
  }
}

class ClimateTheme extends StatefulWidget {
  final Widget child;
  final ClimateThemeKeys initialThemeKey;

  const ClimateTheme({
    Key key,
    this.initialThemeKey,
    @required this.child,
  }) : super(key: key);

  @override
  ClimateThemeState createState() => new ClimateThemeState();

  static ThemeData of(BuildContext context) {
    _ClimateTheme inherited =
        (context.inheritFromWidgetOfExactType(_ClimateTheme) as _ClimateTheme);
    return inherited.data.theme;
  }

  static ClimateThemeState instanceOf(BuildContext context) {
    _ClimateTheme inherited =
        (context.inheritFromWidgetOfExactType(_ClimateTheme) as _ClimateTheme);
    return inherited.data;
  }
}

class ClimateThemeState extends State<ClimateTheme> {
  ThemeData _theme;

  ThemeData get theme => _theme;

  @override
  void initState() {
    _theme = ClimateThemes.getThemeFromKey(widget.initialThemeKey);
    super.initState();
  }

  void changeTheme(ClimateThemeKeys themeKey) {
    setState(() {
      _theme = ClimateThemes.getThemeFromKey(themeKey);
    });
  }

  @override
  Widget build(BuildContext context) {
    return new _ClimateTheme(
      data: this,
      child: widget.child,
    );
  }
}
