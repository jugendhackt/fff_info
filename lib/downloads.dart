import 'package:fff_info/api.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:share/share.dart';

import 'globals.dart';
import 'responsiveDrawerScaffold.dart';

class Downloads extends StatefulWidget {
  Downloads({
    Key key,
  }) : super(key: key);

  @override
  _DownloadState createState() => _DownloadState();
}

class _DownloadState extends State<Downloads> {
  bool _contentLoaded = false;
  List<PetitionDocument> _documents;
  Set _documentTypes = Set();
  Set _documentLanguages = Set();

  String _languageFilter = 'All';
  String _typeFilter = 'All';

  @override
  void initState() {
    if (!_contentLoaded)
      GApi.documentData().then((myList) {
        myList.forEach((document) {
          _documentTypes.add(document.name);
          _documentLanguages.add(document.language);
        });
        setState(() {
          _documents = myList;
          _contentLoaded = true;
        });
      });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ResponsiveDrawerScaffold(
        title: 'Documents',
        body: (_contentLoaded)
            ? () {
                // Preparing to filter by document type
                List<PetitionDocument> filteredDocuments = List();
                if (_typeFilter == 'All') {
                  filteredDocuments.addAll(_documents);
                } else {
                  filteredDocuments.addAll(_documents
                      .where((element) => element.name == _typeFilter));
                }
                // Preparing to filter by language
                if (_languageFilter != 'All')
                  filteredDocuments.removeWhere(
                      (element) => element.language != _languageFilter);
                return ListView.builder(
                    itemCount: filteredDocuments.length + 1,
                    itemBuilder: (BuildContext context, int _i) {
                      if (_i == 0) {
                        Set documentTypes = Set.from(['All']);
                        documentTypes.addAll(_documentTypes);
                        Set languages = Set.from(['All']);
                        languages.addAll(_documentLanguages);
                        return Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: (Column(
                            children: <Widget>[
                              Text(
                                'Petition sheets',
                                style: Theme.of(context).textTheme.headline4,
                              ),
                              Text(
                                  'If you want to engange in your local #FFF community, you may share these petition sheets and hand them back to your local #FFF administration. Please select the language to get started.'),
                              Wrap(
                                alignment: WrapAlignment.start,
                                crossAxisAlignment: WrapCrossAlignment.center,
                                spacing: 8,
                                children: <Widget>[
                                  Text('Document type: '),
                                  DropdownButton(
                                    onChanged: (newFilter) => setState(() {
                                      _typeFilter = newFilter;
                                    }),
                                    value: _typeFilter,
                                    items: documentTypes
                                        .map<DropdownMenuItem<String>>(
                                            (e) => DropdownMenuItem(
                                                  child: Text(e),
                                                  value: e,
                                                ))
                                        .toList(),
                                  ),
                                  Text('Document language: '),
                                  DropdownButton(
                                    onChanged: (newFilter) => setState(() {
                                      _languageFilter = newFilter;
                                    }),
                                    value: _languageFilter,
                                    items: languages
                                        .map<DropdownMenuItem<String>>(
                                            (e) => DropdownMenuItem(
                                                  child: Text(e),
                                                  value: e,
                                                ))
                                        .toList(),
                                  ),
                                ],
                              ),
                              Divider()
                            ],
                          )),
                        );
                      }
                      return new ListTile(
                        leading: Icon(FontAwesomeIcons.download),
                        title: Text(filteredDocuments[_i - 1].name +
                            ', ' +
                            filteredDocuments[_i - 1].language),
                        subtitle: Text('Long press to share'),
                        onTap: filteredDocuments[_i - 1].download,
                        onLongPress: () =>
                            Share.share(filteredDocuments[_i - 1].url),
                      );
                    });
              }()
            : Center(child: CircularProgressIndicator()));
  }
}
