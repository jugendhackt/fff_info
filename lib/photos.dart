import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'responsiveDrawerScaffold.dart';

class PhotoPage extends StatefulWidget {
  PhotoPage({
    Key key,
  }) : super(key: key);

  @override
  _PhotoPagePageState createState() => _PhotoPagePageState();
}

class _PhotoPagePageState extends State<PhotoPage> {
  Future<List> Start() async {
    List ret = new List();

    final response =
        await http.get('https://www.flickr.com/photos/350org/albums');
    if (response.statusCode == 200) {
      RegExp regex = new RegExp(
          r'src\":\"\\\/\\\/live\.staticflickr\.com\\\/.....\\\/......................_q\.jpg');
      Iterable<RegExpMatch> selected = await regex.allMatches(response.body);

      for (var i in selected) {
        print(response.body
            .substring(i.start + 10, i.end)
            .replaceAll(new RegExp(r'\\'), r''));
        await ret.add({
          "url": "https://" +
              response.body
                  .substring(i.start + 10, i.end)
                  .replaceAll(new RegExp(r'\\'), r'')
        });
      }
    } else
      print("FAILED to get http");

    return ret;
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> widgets = new List<Widget>();

    if (!contentLoaded)
      Start().then((myList) {
        setState(() {
          print(myList.length);
          content = GridView.builder(
              itemCount: myList.length,
              gridDelegate:
                  SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
              itemBuilder: (BuildContext context, int _i) {
                return InkWell(
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) =>
                            FullScreenImage(myList[_i]["url"])));
                  },
                  child: Card(
                    child: Image.network(
                      myList[_i]["url"],
                      scale: 0.5,
                    ),
                  ),
                );
              });
          contentLoaded = true;
        });
      });

    return ResponsiveDrawerScaffold(title: 'Gallery', body: content);
  }

  Widget content = Center(child: CircularProgressIndicator());
  bool contentLoaded = false;
}

class FullScreenImage extends StatelessWidget {
  final String url;
  FullScreenImage(this.url);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Image'),
      ),
      body: Center(
        child: Image.network(
          url,
          scale: 0.1,
        ),
      ),
    );
  }
}
