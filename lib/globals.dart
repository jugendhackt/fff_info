import 'package:flutter/material.dart';

import 'api.dart';

const W_PRIMARY = Color.fromRGBO(78, 229, 139, 1);
// Color.fromRGBO(27, 115, 64, 1); for dark green
const G_PRIMARY = Color.fromRGBO(29, 166, 74, 1);
const W_SECONDARY = Color.fromRGBO(255, 255, 51, 1);
const G_SECONDARY = Color.fromRGBO(144, 211, 237, 1);
const W_BLACK = Color.fromRGBO(35, 31, 32, 1);
const G_BLACK = Colors.black;
const WHITE = Colors.white;

const String FLICKR_API_URL_OAUTH_BASE =
    "https://www.flickr.com/services/oauth";
const String FLICKR_API_URL_REQUEST_TOKEN =
    "$FLICKR_API_URL_OAUTH_BASE/request_token";
const String FLICKR_API_URL_AUTHORIZE = "$FLICKR_API_URL_OAUTH_BASE/authorize";
const String FLICKR_API_URL_ACCESS_TOKEN =
    "$FLICKR_API_URL_OAUTH_BASE/access_token";
const String API_KEY = '88b785f630b12fd2ad3ed5e9d44d21d5';
const String API_SECRET = 'e6a0990b01dbc66e';
String LANG = "en";
Api GApi = new Api();
