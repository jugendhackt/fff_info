import 'package:fff_info/globals.dart';
import 'package:fff_info/theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'globals.dart';
import 'i18n.dart';
import 'responsiveDrawerScaffold.dart';

class SettingsPage extends StatefulWidget {
  SettingsPage({
    Key key,
  }) : super(key: key);

  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<SettingsPage> {
  String _selectedTheme = 'global';
  String _selectedLanguage = 'english';

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return ResponsiveDrawerScaffold(
        title: Internationalisation.of(context).translation("settings"),
        body: Column(children: <Widget>[
          Center(
              child: Card(
                  elevation: 8.0,
                  child: Container(
                      padding: EdgeInsets.all(10.0),
                      child: Column(children: <Widget>[
                        Text(
                          Internationalisation.of(context)
                              .translation("settings"),
                          style: Theme.of(context).textTheme.display1,
                        ),
                        ListTile(
                          leading: Icon(FontAwesomeIcons.adjust),
                          title: Row(
                            children: <Widget>[
                              Text('Theme:'),
                              CupertinoSegmentedControl(
                                groupValue: _selectedTheme,
                                selectedColor:
                                    ClimateTheme.of(context).primaryColor,
                                unselectedColor: ClimateTheme.of(context)
                                    .primaryTextTheme
                                    .body1
                                    .color,
                                children: {
                                  'global': Text(
                                      Internationalisation.of(context)
                                          .translation("global")),
                                  'germany': Text(
                                      Internationalisation.of(context)
                                          .translation("germany"))
                                },
                                onValueChanged: (value) {
                                  var theme;
                                  switch (value) {
                                    case 'global':
                                      theme = ClimateThemeKeys.WORLD;
                                      break;
                                    case 'germany':
                                      theme = ClimateThemeKeys.GERMANY;
                                      break;
                                    default:
                                      theme = ClimateThemeKeys.WORLD;
                                      break;
                                  }
                                  ClimateTheme.instanceOf(context)
                                      .changeTheme(theme);
                                  setState(() {
                                    _selectedTheme = value;
                                  });
                                },
                              ),
                            ],
                          ),
                        ),
                        ListTile(
                          leading: Icon(FontAwesomeIcons.flag),
                          title: Row(
                            children: <Widget>[
                              Text(Internationalisation.of(context)
                                  .translation("language")),
                              CupertinoSegmentedControl(
                                groupValue: _selectedLanguage,
                                selectedColor:
                                    ClimateTheme.of(context).primaryColor,
                                unselectedColor: ClimateTheme.of(context)
                                    .primaryTextTheme
                                    .body1
                                    .color,
                                children: {
                                  'english': Text(
                                      Internationalisation.of(context)
                                          .translation("english")),
                                  'german': Text(
                                      Internationalisation.of(context)
                                          .translation("german"))
                                },
                                onValueChanged: (value) {
                                  var theme;
                                  switch (value) {
                                    case 'german':
                                      LANG = "de";
                                      break;
                                    case 'english':
                                      LANG = "en";
                                      break;
                                    default:
                                      LANG = "en";
                                      break;
                                  }
                                  ClimateTheme.instanceOf(context)
                                      .changeTheme(theme);
                                  setState(() {
                                    _selectedLanguage = value;
                                  });
                                },
                              ),
                            ],
                          ),
                        ),
                      ]))))
        ]));
  }
}
