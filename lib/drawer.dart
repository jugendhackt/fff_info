import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'downloads.dart';
import 'excuse.dart';
import 'photos.dart';
import 'questions.dart';
import 'settings.dart';
import 'strikelist.dart';

class ClimateDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        color: Colors.white,
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            UserAccountsDrawerHeader(
              accountName: Text(
                "#FFF Info",
                style: Theme.of(context).textTheme.display2,
              ),
              accountEmail: Text('Strike for our climate!'),
              currentAccountPicture: Image.asset(
                "assets/launcher.png",
                semanticLabel: 'Fridays for future logo',
              ),
              decoration: BoxDecoration(color: Theme.of(context).primaryColor),
            ),
            ListTile(
              title: Text('Strike List'),
              leading: Icon(FontAwesomeIcons.listAlt),
              onTap: () {
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(builder: (context) => StrikeListPage()),
                );
              },
            ),
            ListTile(
              title: Text('Pictures'),
              leading: Icon(FontAwesomeIcons.photoVideo),
              onTap: () {
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(builder: (context) => PhotoPage()),
                );
              },
            ),
            ListTile(
              title: Text('FAQ'),
              leading: Icon(FontAwesomeIcons.question),
              onTap: () {
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(builder: (context) => QuestionsPage()),
                );
              },
            ),
            ListTile(
              title: Text('Documents'),
              leading: Icon(FontAwesomeIcons.download),
              onTap: () {
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(builder: (context) => Downloads()),
                );
              },
            ),
            Tooltip(
              message: 'Still in development. Expect issues.',
              child: ListTile(
                title: Text('Excuse Generator'),
                leading: Icon(FontAwesomeIcons.paragraph),
                onTap: () {
                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(builder: (context) => ExcusePage()),
                  );
                },
              ),
            ),
            ListTile(
              title: Text('Settings'),
              leading: Icon(FontAwesomeIcons.slidersH),
              onTap: () {
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(builder: (context) => SettingsPage()),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
