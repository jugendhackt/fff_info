import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:url_launcher/url_launcher.dart';

class Api {
  List<PetitionDocument> _documents = new List();

  List get documents => _documents;

  set documents(List value) {
    _documents = value;
  }

  Future<List> eventData() async {
    const String url = (kIsWeb)
        ? 'https://fridays.cf/map'
        : 'https://www.fridaysforfuture.org/events/map'; // removing `?d=All` from url since this amount of data kills any mobile CPU :wow:
    final response = await http.get(url);

    if (response.statusCode == 200) {
      var regex = new RegExp(r"eventmap_data \= (.*);");
      String line = regex.firstMatch(response.body).group(1);
      List eventData = jsonDecode(line);
      return (eventData);
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to load post');
    }
  }

  Future<String> _httpRequest(String url) async {
    final response = await http.get(url);

    if (response.statusCode == 200) {
      return response.body;
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to load post');
    }
  }

  Future<bool> checkInternetConnection() async {
    return (await _httpRequest("http://detectportal.firefox.com/")) ==
        'success';
  }

  Future<List<PetitionDocument>> documentData() async {
    final response1 = await http.get('https://fridays.cf/eci/');
    if (response1.statusCode == 200) {
      RegExp regex = new RegExp(
          r'href\=\"https://eci.fridaysforfuture.org/wp-content/uploads/[0-9]{4}/10/ECI-Support-Statements-Form-253-[A-Z]{2}-[A-Z]{2}.pdf\">');
      Iterable<RegExpMatch> selected = regex.allMatches(response1.body);

      for (var i in selected) {
        _documents.add(PetitionDocument(
            url: response1.body.substring(i.start + 6, i.end - 2),
            name: 'Petition sheet',
            language: response1.body.substring(i.start + 98, i.end - 9)));
      }
    } else
      print("FAILED to get http");

    final response2 = await http.get(
        'https://www.dropbox.com/sh/ivctbx9t8kq5obq/AAA_z1p_22EY5-O7N_M9IRESa?dl=0');
    if (response2.statusCode == 200) {
      RegExp regex = new RegExp(
          r'https:\/\/www\.dropbox\.com\/sh\/[a-zA-Z0-9]{15}\/[a-zA-Z0-9]{25}\/.([0-9a-zA-Z]|-){5,25}\.pdf');
      Iterable<RegExpMatch> selected = regex.allMatches(response2.body);

      for (var i in selected) {
        var label = response2.body.substring(i.start + 69, i.end - 4);
        _documents.add(PetitionDocument(
            url: response2.body.substring(i.start, i.end),
            name: label.substring(0, label.length - 2).replaceAll('-', ''),
            language: label.substring(label.length - 2)));
      }
    } else
      print("FAILED to get http");

    return _documents;
  }
}

class PetitionDocument {
  String language;
  String name;
  final String url;
  PetitionDocument(
      {@required this.name, @required this.language, @required this.url}) {
    language = language.toUpperCase();
  }

  void download() async {
    // Error due to url ending `PDF`...
    if (url.contains('dropbox')) {
      launch(url + '/');
    } else {
      launch(url);
    }
  }

  void copy() async {
    Clipboard.setData(ClipboardData(text: url));
  }
}
