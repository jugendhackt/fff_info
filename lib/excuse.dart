import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:share/share.dart';

import 'responsiveDrawerScaffold.dart';

class ExcusePage extends StatefulWidget {
  ExcusePage({
    Key key,
  }) : super(key: key);

  @override
  _ExcusePageState createState() => _ExcusePageState();
}

class _ExcusePageState extends State<ExcusePage> {
  static DateTime selectedDate = DateTime.now();
  static TimeOfDay selectedTime = TimeOfDay.now();

  String _PDFText = "";
  String datenow = DateTime.now().day.toString() +
      "." +
      DateTime.now().month.toString() +
      "." +
      DateTime.now().year.toString();

  static String foaValue = "Mrs.",
      childValue = "Son",
      date = selectedDate.day.toString() +
          "." +
          selectedDate.month.toString() +
          "." +
          selectedDate.year.toString(),
      timeOfLeave =
          selectedTime.hour.toString() + ":" + selectedTime.minute.toString(),
      timeOfStart =
          selectedTime.hour.toString() + ":" + selectedTime.minute.toString();

  String fOA = "Mrs.";
  String nOT = "";
  String nOP = "";
  String dOS = date;
  String tOS = timeOfStart;
  String tOL = timeOfLeave;
  String cG = "Son";
  String nOC = "";
  String cls = "";
  String pOR = "";
  String ePS = "";
  String cSP = "";

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        date = selectedDate.day.toString() +
            "." +
            selectedDate.month.toString() +
            "." +
            selectedDate.year.toString();

        dOS = date;
      });
  }

  Future<Null> _selectTime(BuildContext context, bool tOfLeave) async {
    final TimeOfDay picked =
        await showTimePicker(context: context, initialTime: selectedTime);

    if (picked != null && picked != selectedTime)
      setState(() {
        selectedTime = picked;
        if (tOfLeave) {
          timeOfLeave = selectedTime.hour.toString() +
              ":" +
              selectedTime.minute.toString();
          tOL = timeOfLeave;
        } else {
          timeOfStart = selectedTime.hour.toString() +
              ":" +
              selectedTime.minute.toString();
          tOS = timeOfStart;
        }
      });
  }

  void createPDF() {
    setState(() {
      _PDFText = """
\\documentclass{article}
\\usepackage[utf8]{inputenc}
\\usepackage{amsmath}
\\usepackage{amstext}

\\title{\\Huge Beurlaubung}

\\begin{document}
\\maketitle

Sehr geehrte Frau $nOT,
\\newline
\\newline hiermit beantrage ich, meine Tochter $nOC, Klasse $cls, am $dOS ab $tOL Uhr zu beurlauben.
\\newline
\\newline Sie wird an diesem Tag nicht am Unterricht teilnehmen, sondern auf eine Demonstration f{\\\"u}r eine lebensrettende Klimapolitik gehen. Am $dOS ab $tOS findet in $cSP, $ePS, eine Demonstration von Sch{\\\"u}ler*innen f{\\\"u}r eine lebensrettende Klimapolitik unter dem Motto #FridaysForFuture statt.
\\newline
\\newline Nach dem Vorbild von und in Solidarit{\\\"a}t mit Greta Thunberg, die jeden Freitag vor dem schwedischen Parlament streikt, ist die Bewegung inzwischen auf der ganzen Welt vertreten.
\\newline
\\newline Der Klimawandel ist l{\\\"a}ngst eine reale Bedrohung f{\\\"u}r unsere Zukunft. Unsere Kinder werden die ersten Leidtragenden des Klimawandels sein. Gleichzeitig sind wir die letzte Generation, die einen katastrophalen Klimawandel noch verhindern kann. Doch unsere Politiker*innen unternehmen nichts, um die Klimakrise abzuwenden.
\\newline
\\newline Die Treibhausgas-Emissionen steigen seit Jahren und noch immer werden Kohle, {\\\"Ö}l und Gas abgebaut und verbrannt. Deswegen geht mein Kind freitags nicht in die Schule. Denn mit jedem Tag, der ungenutzt verstreicht, wird ihre Zukunft aufs Spiel gesetzt!
\\newline
\\newline #FridaysForFuture ist weder an eine Partei noch an eine Organisation gebunden. Die Klimastreik-Bewegung hat ihre eigene Dynamik und wird durch hunderte individuelle junge Menschen getragen, die freitags streiken, eine Website aufbauen, Pressemitteilungen schreiben, Reden halten, Lautsprecher organisieren und vieles mehr.
\\newline
\\newline Die Bewegung hat im Vorfeld schon viel Unterst{\\\"u}tzung erfahren, ist allerdings auch oft auf Ablehnung gesto\\szen, meist aufgrund von Unsicherheit und der Angst vor negativen Konsequenzen.
\\newline
\\newline Mit diesem Antrag m{\\\"o}chte ich klarstellen, dass ich mich mit der rechtlichen Grundlage von Sch{\\\"u}lerstreiks sorgf{\\\"a}ltig auseinandergesetzt habe:
\\newline
\\newline In Artikel 20a des Grundgesetzes ist festgehalten, dass der Staat die nat{\\\"u}rlichen Lebensgrundlagen sch{\\\"u}tzt und Verantwortung f{\\\"u}r die k{\\\"u}nftigen Generationen tr{\\\"a}gt.
\\newline
\\newline Dieser Pflicht den k{\\\"u}nftigen Generationen gegen{\\\"u}ber kommt der Staat allerdings nicht nach, weshalb f{\\\"u}r uns gilt: Wir streiken, bis ihr handelt!
\\newline
\\newline Wir berufen uns bei unserem Streik vor allem auf unser Recht auf Demonstrationsfreiheit, welches jedem Menschen in Deutschland zusteht (GG, Art. 8). Au\\szerdem ist in der allgemeinen
\\newline
\\newline Schulordnung die Meinungsfreiheit aller Sch{\\\"u}ler*innen festgehalten (AschO, § 36).
\\newline
\\newline Da die Demonstration eine vom Grundgesetz gesch{\\\"u}tzte Form der Meinungs{\\\"a}u\\szerung ist, die allen Sch{\\\"u}ler*innen zusteht (GG, Art. 8), und sich der Streik au\\szerdem f{\\\"u}r eine Politik stark macht, welche unser aller {\\\"U}berleben auf diesem Planeten sichert, kann mein Kind den staatlichen Bildungsauftrag dort am dOS besser wahrnehmen, als in der Schule selbst. Ich nehme zur Kenntnis, dass es sich bei der Demonstration nicht um eine Schulveranstaltung handelt und die Lehrer*innen keine Aufsichtspflicht haben. Der Veranstalter stellt f{\\\"u}r die Dauer der Versammlung vollj{\\\"a}hrige Ordner*innen.
\\newline
\\newline Ich bitte darum, unter diesen besonderen Umst{\\\"a}nden von einer Eintragung unentschuldigter Fehlstunden abzusehen.
\\newline
\\newline Mit freundlichen Gr{\\\"u}\\szen,
\\newline
\\newline $nOP
\\newline
\\newline $pOR, den $datenow 
\\newline
\\newline
\\newline ------------------------------- 
\\newline
\\newline Unterschrift der/des Erziehungsberechtigten

\\end{document}""";
    });
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.

    final key = new GlobalKey<ScaffoldState>();

    return ResponsiveDrawerScaffold(
        title: 'Excuse Generator',
        body: Center(
            child: ListView(
                shrinkWrap: true,
                padding: EdgeInsets.all(15.0),
                children: <Widget>[
              Center(
                  child: Card(
                      elevation: 8.0,
                      child: Container(
                          padding: EdgeInsets.all(10.0),
                          child: Column(children: <Widget>[
                            Text(
                              "Automatic Excuse Generator",
                              style: Theme.of(context).textTheme.display1,
                            ),
                            DropdownButton<String>(
                              value: foaValue,
                              onChanged: (String newValue) {
                                setState(() {
                                  foaValue = newValue;
                                  fOA = foaValue;
                                });
                              },
                              items: <String>[
                                'Mrs.',
                                'Mr.'
                              ].map<DropdownMenuItem<String>>((String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(value),
                                );
                              }).toList(),
                            ),
                            SizedBox(
                              height: 20.0,
                            ),
                            TextField(
                              onChanged: (String newVal) {
                                setState(() {
                                  nOT = newVal;
                                });
                              },
                              decoration: InputDecoration(
                                  hintText: 'Name of the teacher / principal'),
                            ),
                            SizedBox(
                              height: 20.0,
                            ),
                            TextField(
                              onChanged: (String newVal) {
                                setState(() {
                                  nOP = newVal;
                                });
                              },
                              decoration: InputDecoration(
                                  hintText: 'Name of the parent'),
                            ),
                            SizedBox(
                              height: 20.0,
                            ),
                            Text("$date"),
                            SizedBox(
                              height: 5.0,
                            ),
                            RaisedButton(
                              onPressed: () => _selectDate(context),
                              child: Text('Select date of the strike'),
                            ),
                            SizedBox(
                              height: 20.0,
                            ),
                            Text("$timeOfLeave"),
                            SizedBox(
                              height: 5.0,
                            ),
                            RaisedButton(
                              onPressed: () => _selectTime(context, true),
                              child: Text('Select time of leave'),
                            ),
                            SizedBox(
                              height: 20.0,
                            ),
                            Text("$timeOfStart"),
                            SizedBox(
                              height: 5.0,
                            ),
                            RaisedButton(
                              onPressed: () => _selectTime(context, false),
                              child: Text(
                                  'Select time of the beginning of the strike'),
                            ),
                            SizedBox(
                              height: 20.0,
                            ),
                            DropdownButton<String>(
                              value: childValue,
                              onChanged: (String newValue) {
                                setState(() {
                                  childValue = newValue;
                                  cG = childValue;
                                });
                              },
                              items: <String>[
                                'Son',
                                'Daughter'
                              ].map<DropdownMenuItem<String>>((String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(value),
                                );
                              }).toList(),
                            ),
                            SizedBox(
                              height: 20.0,
                            ),
                            TextField(
                              onChanged: (String newVal) {
                                setState(() {
                                  nOC = newVal;
                                });
                              },
                              decoration: InputDecoration(
                                  hintText: 'Name of the child'),
                            ),
                            SizedBox(
                              height: 20.0,
                            ),
                            TextField(
                              onChanged: (String newVal) {
                                setState(() {
                                  cls = newVal;
                                });
                              },
                              decoration: InputDecoration(hintText: 'Class'),
                            ),
                            SizedBox(
                              height: 20.0,
                            ),
                            TextField(
                              onChanged: (String newVal) {
                                setState(() {
                                  pOR = newVal;
                                });
                              },
                              decoration: InputDecoration(
                                  hintText: 'Place of residence'),
                            ),
                            SizedBox(
                              height: 20.0,
                            ),
                            TextField(
                              onChanged: (String newVal) {
                                setState(() {
                                  ePS = newVal;
                                });
                              },
                              decoration: InputDecoration(
                                  hintText: 'Exact place of the strike'),
                            ),
                            SizedBox(
                              height: 20.0,
                            ),
                            TextField(
                              onChanged: (String newVal) {
                                setState(() {
                                  cSP = newVal;
                                });
                              },
                              decoration: InputDecoration(
                                  hintText:
                                      'City where the strike takes place'),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: RaisedButton(
                                onPressed: () => createPDF(),
                                child: Text("Generate excuse!"),
                              ),
                            ),
                            Text(
                              'What is this!?\nIt\'s LaTeX code. You can paste it in any online generator you find by searching \'online latex to pdf\'.',
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyText1
                                  .copyWith(fontWeight: FontWeight.bold),
                            ),
                            Divider(),
                            Tooltip(
                                message: 'Tap to share',
                                child: SelectableText(
                                  _PDFText,
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyText1
                                      .copyWith(fontFamily: 'Katwijk'),
                                  onTap: () {
                                    Share.share(_PDFText);
                                  },
                                )),
                          ]))))
            ])));
  }
}
